@echo off

if not exist workshop md workshop

call gradients_gen.bat

for %%a in (source\*.png) do (
  echo Processing %%~nxa
  call texture_emblem.bat %%a textures
)

call montage.bat textures\dds workshop 11x

del textures\temp\gradient_fill.png
del textures\temp\gradient_highlight.png

rmdir textures\temp
