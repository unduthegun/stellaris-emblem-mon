
::big montage with names
montage.exe textures\*.dds -set label %%t -tile 11x -geometry 128x128+1+1 -background black -fill white -font YanoneKaffeesatz-Bold -pointsize 16 workshop\montage.png

::small montage
montage.exe textures\small\*.dds -tile 11x -geometry 24x24+1+1 -background white workshop\montage_small.png
